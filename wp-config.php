<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $_SERVER['DB_NAME']);

/** MySQL database username */
define('DB_USER', $_SERVER['DB_USER']);

/** MySQL database password */
define('DB_PASSWORD', $_SERVER['DB_PASS']);

/** MySQL hostname */
define('DB_HOST', $_SERVER['DB_HOST']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'AWS_ACCESS_KEY_ID', $_SERVER['AWS_ACCESS_KEY_ID']);
define( 'AWS_SECRET_ACCESS_KEY', $_SERVER['AWS_SECRET_ACCESS_KEY']);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';s_/kb@`K=_8@V%ZSq%&+jqJ|S$d9W:|`$H=i7M)+{V-9q2J^)qb_!e-/|+8M0r*');
define('SECURE_AUTH_KEY',  'G[Q66& INJl5k!2!-~#tuNOw+v>fc(7X5Cc$^{fVV +AcO`:D&NKqFZLuo^zoX+e');
define('LOGGED_IN_KEY',    'T+.i5}x?>*^dK-DJpaRi$N3]n0:F9PY-P!t4+j*|Q]Lqbb]by9Dom]W:YNvA.VUL');
define('NONCE_KEY',        '#:/,+!->0{)!Qf0epXb37:Qw2=tPm$DAS] ;<xj!f|Dz90I^W-6B*S&|B%(DoW2j');
define('AUTH_SALT',        '^l905+.1$2Z&yTSLJOB b(gCBMh)iY%YhGM1u)-<20-:T+Q(4~v?zIdf5RH;_-<#');
define('SECURE_AUTH_SALT', 'z5nT@_Mz6-5?7^8c_-/xx1(o8CuIoyJ1|/b!.R;-W+:k2^5`{A0u7YV*5&ra7EG+');
define('LOGGED_IN_SALT',   'YS~92d a6Q|njg|.<6i7uJ+[nqE}phnR=};h]uKB(UGQv;**|B|rl$qeEEeR-8^0');
define('NONCE_SALT',       '|igiF=$9}nD_r=.Q*&&NodY-8Nce$f}*kC|=]/JGb~=90s:t=C!7DB|6`cIM)R?`');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
